export class Task {
    taskId: number
    taskName: string
    type: string
    dueDate: Date
}
