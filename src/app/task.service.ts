import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from './Task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private viewUrl = 'https://todo-cqrs-view-service.herokuapp.com'
  private updateUrl = 'https://todo-cqrs-update-service.herokuapp.com'

  constructor(private http: HttpClient) { }

  findAll(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.viewUrl}/getTasks`)
  }

  save(task: Task) {
    return this.http.post(`${this.updateUrl}/addTask`, task)
  }

  update(task: Task) {
    return this.http.put(`${this.updateUrl}/updateTask`, task)
  }

  delete(taskId: number) {
    return this.http.delete(`${this.updateUrl}/deleteTask/${taskId}`)
  }

}
