import { Component, OnInit, Inject } from '@angular/core';
import { TaskService } from './task.service';
import { Task } from './Task';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  task: Task = new Task()
  tasks: Task[] = []
  minDate: Date = new Date()

  //Flags required for UI
  isLoading: boolean = true
  isAdding: boolean = false

  constructor(private taskService: TaskService, public dialog: MatDialog, private snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.taskService.findAll().subscribe(res => {
      this.tasks = res
      this.isLoading = false
    })
  }

  addTask() {
    this.isAdding = true
    this.taskService.save(this.task).subscribe(res => {
      this.ngOnInit()
      this.isAdding = false
      this.snackBar.open('Task added...', 'Ok', { duration: 4500 })
    })
  }

  delete(taskId: number) {
    this.taskService.delete(taskId).subscribe(res => {
      this.snackBar.open("Task deleted...", "Ok", { duration: 4800 })
      this.ngOnInit()
    })
  }

  openDialog(task: Task) {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '480px',
      data: {
        taskId: task.taskId,
        taskName: task.taskName,
        type: task.type,
        dueDate: task.dueDate
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit()
    });
  }
}
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'update-popup.html',
})
export class DialogOverviewExampleDialog {

  minDate: Date = new Date()

  constructor(public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Task, private taskService: TaskService,
    private snackBar: MatSnackBar) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  update(form: NgForm) {
    this.taskService.update(this.data).subscribe(res => {
      this.snackBar.open("Task Updated...", "Ok", { duration: 5000 })
      this.onNoClick()
    })
  }
}
